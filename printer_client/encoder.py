import hashlib
from typing import Optional, List
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.PassThruCardService import PassThruCardService
from logging import getLogger

LOG = getLogger(__name__)

encoders_to_commands = {
    "SDI011": {
        "READ_UUID": [0xFF, 0xCA, 0x00, 0x00, 0x00],
        "RESPONSE_SUCCESS": 0x90,
        "LOAD_READ_KEY": lambda read_key: [0xFF, 0x82, 0x00, 0x60, 0x06, *read_key],
        "LOAD_WRITE_KEY": lambda write_key: [0xFF, 0x82, 0x00, 0x61, 0x06, *write_key],
        "AUTHENTICATE_READ_KEY": lambda block: [
            0xFF,
            0x86,
            0x00,
            0x00,
            0x05,
            0x01,
            0x00,
            block,
            0x60,
            0x00,
        ],  # noqa: E501
        "AUTHENTICATE_WRITE_KEY": lambda block: [
            0xFF,
            0x86,
            0x00,
            0x00,
            0x05,
            0x01,
            0x00,
            block,
            0x61,
            0x01,
        ],  # noqa: E501
        "READ_BLOCK": lambda block: [0xFF, 0xB0, 0x00, block, 0x10],
        "WRITE_BLOCK": lambda block, data: [0xFF, 0xD6, 0x00, block, 0x10, *data],
    },
    "ACR1252": {
        "READ_UUID": [0xFF, 0xCA, 0x00, 0x00, 0x00],
        "RESPONSE_SUCCESS": 0x90,
        "LOAD_READ_KEY": lambda read_key: [0xFF, 0x82, 0x00, 0x00, 0x06, *read_key],
        "LOAD_WRITE_KEY": lambda write_key: [0xFF, 0x82, 0x00, 0x01, 0x06, *write_key],
        "AUTHENTICATE_READ_KEY": lambda block: [
            0xFF,
            0x86,
            0x00,
            0x00,
            0x05,
            0x01,
            0x00,
            block,
            0x60,
            0x00,
        ],  # noqa: E501
        "AUTHENTICATE_WRITE_KEY": lambda block: [
            0xFF,
            0x86,
            0x00,
            0x00,
            0x05,
            0x01,
            0x00,
            block,
            0x61,
            0x01,
        ],  # noqa: E501
        "READ_BLOCK": lambda block: [0xFF, 0xB0, 0x00, block, 0x10],
        "WRITE_BLOCK": lambda block, data: [0xFF, 0xD6, 0x00, block, 0x10, *data],
    },
}


def to_bytearray(hex_string: str) -> List[int]:
    """Convert a hexstring to a bytearray of width 16"""

    return bytearray([int(hex_string[i : i + 2], 16) for i in range(0, len(hex_string), 2)])


def to_hexstring(input) -> str:
    """Convert input (str or int) to hex and fill to width 16"""

    if type(input) is str:
        value = input.encode("utf-8").hex()
        value = value.ljust(16 * 2, "0")
        return value
    elif type(input) is int:
        value = input.to_bytes(16, "little")
        value = value.hex()
        return value
    else:
        LOG.debug(f"to_hexstring() returned None for input: {input}")
        return None


def to_ascii(input: List[int]) -> str:
    """Attempt to convert a byte array into ascii"""

    try:
        while input[-1] == 0:
            input.pop()
    except IndexError:
        input = []

    data_hex = "".join("{:02x}".format(x) for x in input)

    try:
        data_ascii = bytearray.fromhex(data_hex).decode()
    except UnicodeDecodeError:
        data_ascii = None

    return data_ascii


def to_integer(input: List[int], byteorder="little") -> int:
    """Attempt to convert a byte array to an integer"""

    data_int = int.from_bytes(input, byteorder=byteorder)

    return data_int


def merge_data(left_block: List[dict], right_block: List[dict]) -> List[dict]:
    """
    For each item in left, merge item['data'] into right matching on `block`.
    Example: merge_data(cardholder_rfid_data, rfid_key_configuration)

    """

    left = left_block.copy()
    right = right_block.copy()

    left_block_ids = set([item["block"] for item in left])
    right_block_ids = set([item["block"] for item in right])

    if not left_block_ids.issubset(right_block_ids):
        block_diff = left_block_ids - right_block_ids
        raise ValueError(f"left blocks {block_diff} is not a subset of right blocks")

    for right_item in right:
        left_item = next(
            (left_item for left_item in left if (left_item["block"] == right_item["block"])),
            None,
        )

        if left_item and left_item["data"]:
            right_item["data"] = left_item["data"]

    return right


def generate_uid_hash(uid: int) -> str:
    """Returns a hash of the card uid"""

    uid_hash = int(hashlib.sha256(str(uid).encode("utf-8")).hexdigest(), 16) % 10**8

    return uid_hash


def read_card_uid(card_service: PassThruCardService, hardware: str = "SDI011") -> int:
    """Return the card_uid (also called Mifare ID) from a physical card"""

    if hardware not in list(encoders_to_commands.keys()):
        raise RuntimeError(f"Invalid hardware: {hardware}")

    encoder_commands = encoders_to_commands[hardware]

    data, response_code, _ = card_service.connection.transmit(encoder_commands["READ_UUID"])
    if response_code != encoder_commands["RESPONSE_SUCCESS"]:
        return None
    return int.from_bytes(data, byteorder="little")


def block_operation(
    cardservice: PassThruCardService,
    data: str,
    block: int,
    read_key: str,
    write_key: str,
    auth_with: str,
    operation: str = "read",
    hardware: str = "SDI011",
):
    """Write/read to a RFID block using Identiv SDI011 encoder (default)"""

    data = to_bytearray(data)
    read_key = to_bytearray(read_key)
    write_key = to_bytearray(write_key)

    if auth_with not in ["readkey", "writekey"]:
        raise RuntimeError(f"Invalid auth_with: {auth_with}")

    if operation not in ["read", "write"]:
        raise RuntimeError(f"Invalid operation: {operation}")

    if hardware not in list(encoders_to_commands.keys()):
        raise RuntimeError(f"Invalid hardware: {hardware}")

    encoder_commands = encoders_to_commands[hardware]

    LOG.debug(f"block: {block}, auth_with: {auth_with}, data: {data}")

    if auth_with == "readkey":
        # load read key
        _, response_code, _ = cardservice.connection.transmit(
            encoder_commands["LOAD_READ_KEY"](read_key)
        )

        if response_code != encoder_commands["RESPONSE_SUCCESS"]:
            raise RuntimeError("Failed to load read key")

        # authenticate read key
        _, response_code, _ = cardservice.connection.transmit(
            encoder_commands["AUTHENTICATE_READ_KEY"](block)
        )
        if response_code != encoder_commands["RESPONSE_SUCCESS"]:
            raise RuntimeError("Unable to authenticate with read key")

    if auth_with == "writekey":
        # load write key key
        _, response_code, _ = cardservice.connection.transmit(
            encoder_commands["LOAD_WRITE_KEY"](write_key)
        )
        if response_code != encoder_commands["RESPONSE_SUCCESS"]:
            raise RuntimeError("Failed to load write key")

        # authenticate write key
        _, response_code, _ = cardservice.connection.transmit(
            encoder_commands["AUTHENTICATE_WRITE_KEY"](block)
        )
        if response_code != encoder_commands["RESPONSE_SUCCESS"]:
            raise RuntimeError("Unable to authenticate with write key")

    if operation == "read":
        # attempt to read data
        return_data, response_code, _ = cardservice.connection.transmit(
            encoder_commands["READ_BLOCK"](block)
        )

    if operation == "write":
        # attempt to write data
        return_data, response_code, _ = cardservice.connection.transmit(
            encoder_commands["WRITE_BLOCK"](block, data)
        )

    if response_code != encoder_commands["RESPONSE_SUCCESS"]:
        raise RuntimeError(f"Failed to {operation} data")

    return return_data


def card_operation(
    blocks: List[dict],
    last_card_uid: Optional[int],
    operation: str = "read",
    hardware: str = "SDI011",
) -> str:
    """Write/read to a Mifare 4K card using the SDI011 encoder (default)"""
    card_uid: Optional[int] = None
    card_request = CardRequest(timeout=None, cardType=AnyCardType())
    cardservice = card_request.waitforcard()

    LOG.debug(f"Using hardware profile: {hardware}")

    try:
        cardservice.connection.connect()
        card_uid = read_card_uid(cardservice)

    except Exception:
        pass

    if not card_uid or card_uid == last_card_uid:
        cardservice.connection.disconnect()
        return card_uid or last_card_uid

    LOG.info(f"card_uid: {card_uid}, operation: {operation}")

    for block in blocks:
        if not block["data"] or block["data"] == "":
            # Identiv SDI011 throws an error on '' ACR1252 does not.
            block["data"] = "00000000000000000000000000000000"

        try:
            LOG.debug(f"card UID: {card_uid}, block: {block['block']}")
            return_data = block_operation(
                cardservice,
                block["data"],
                block["block"],
                block["readkey"],
                block["writekey"],
                block["authwith"],
                operation=operation,
                hardware=hardware,
            )
        except Exception as err:
            LOG.error(f"card UID: {card_uid}, block: {block['block']}, {err}")
            cardservice.connection.disconnect()
            raise err

        if operation == "read":
            LOG.info(f"block: {str(block['block']).zfill(2)}, return data: {return_data}")
            LOG.info(
                f"block: {str(block['block']).zfill(2)}, "
                f"ascii: {to_ascii(return_data)}, int (little): {to_integer(return_data)}"
            )

    cardservice.connection.disconnect()

    return card_uid
