"""
University Card Printer Client

Usage:
    printer_client (-h | --help)
    printer_client run [--config=FILE] [--quiet] [--verbose] [--encode-blank-cards]

Options:
    -h, --help                  Show usage summary.
    -v, --verbose               Log debugging information.
    -c, --config=FILE           Specify optional configuration file to use [default: config.yml].
    -e, --encode-blank-cards    Encode blank cards with MIFARE_NUMBER only.

"""

import os
import logging
import time
from docopt import docopt

from identitylib.identifiers import IdentifierSchemes

from printer_client.utils import load_settings
from printer_client.card_request_client import CardRequestClient
from printer_client.printer import create_card, encode_blank_card


LOG = logging.getLogger(__name__)


def get_number_of_blank_cards():
    """Get the number of blank cards to be encoded."""

    while True:
        try:
            number_of_cards = int(input("Number of blank cards to encode:"))
            break
        except ValueError:
            print("Please input an integer value.")
            continue
    return number_of_cards


def encode_blank_cards(settings, card_client, number_of_cards=None):
    """
    Encode blank cards generating a MIFARE_NUMBER from a hash of the card UID
    set by the card manufacturer.

    """
    if not number_of_cards:
        number_of_cards = get_number_of_blank_cards()

    rfid_data_configuration = settings.get(
        "CARD_API_RFID_DATA_CONFIGURATION", "MIFARE_CLASSIC_4K_V1"
    )

    # Only the card keys and blank sectors are recorded in rfid_sector_data at this point.
    # The card UID is read mid-process and from this card UID a MIFARE_NUMBER is generated.
    # The MIFARE_NUMBER is merged with rfid_sector_data prior to writing to the card.

    rfid_sector_data = card_client.get_card_rfid_data_config(rfid_data_configuration)

    for card_num in range(number_of_cards):
        try:
            card_uid = encode_blank_card(rfid_sector_data)
            LOG.info(f"({card_num + 1} of {number_of_cards}) encoded card id: {card_uid}")
        except Exception as e:
            LOG.error(f"Printer client returned exception: {e}")
            break


def encode_and_print_card_requests(settings, card_client):
    """Encode and print card requests fetched from the Card API"""

    rfid_data_format = settings.get("CARD_API_RFID_DATA_FORMAT", "MIFARE_V1")

    while True:
        (
            card_request_details,
            merged_rfid_data,
            front_image,
            back_image,
        ) = card_client.get_next_card_request(
            rfid_data_format
        )  # noqa: E501

        if card_request_details is None:
            LOG.info(
                "Nothing to print, "
                f"next check in {settings.get('PRINTER_CLIENT_SLEEP_DURATION', 30)}s"
            )  # noqa: E501
            time.sleep(settings.get("PRINTER_CLIENT_SLEEP_DURATION", 30))
            continue

        try:
            card_uid = create_card(merged_rfid_data, front_image, back_image)
        except Exception as e:
            LOG.error(
                f"Card request {card_request_details['id']} printing returned exception: {e}"
            )
            card_client.update_card_request(card_request_details["id"], {"action": "requeue"})
        else:
            card_client.update_card_request(
                card_request_details["id"],
                {
                    "action": "make",
                    "identifiers": [
                        {
                            "value": str(card_uid),
                            "scheme": str(IdentifierSchemes.MIFARE_ID),
                        }
                    ],
                },
            )
        finally:
            for temp_img in [front_image, back_image]:
                try:
                    os.remove(temp_img)
                except OSError:
                    pass


def main():
    opts = docopt(__doc__)

    logging.basicConfig(
        level=logging.DEBUG
        if opts["--verbose"]
        else logging.WARN
        if opts["--quiet"]
        else logging.INFO
    )

    settings = load_settings(opts["--config"])

    card_client = CardRequestClient(
        settings.get("API_GATEWAY_ACCESS_CLIENT_KEY"),
        settings.get("API_GATEWAY_ACCESS_CLIENT_SECRET"),
        settings.get("API_GATEWAY_ACCESS_TOKEN_URL"),
        settings.get("CARD_API_BASE_URL"),
        settings.get("CARD_API_INSTITUTION_FILTER", None),
    )

    if opts["--encode-blank-cards"]:
        encode_blank_cards(settings, card_client)
    else:
        encode_and_print_card_requests(settings, card_client)
