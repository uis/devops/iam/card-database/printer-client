import re
import tempfile
from io import IOBase
from logging import getLogger

from identitylib.card_client import ApiClient, ApiException
from identitylib.card_client.api.v1beta1_api import V1beta1Api as CardApi
from identitylib.card_client_configuration import CardClientConfiguration
from tenacity import (
    retry,
    retry_if_not_exception_type,
    stop_after_attempt,
    wait_exponential,
)

from printer_client.encoder import merge_data, to_hexstring

WAIT_MIN = 3
WAIT_MAX = 10
MAX_ATTEMPT = 5
MULTIPLIER = 1
LOG = getLogger(__name__)


class CardRequestClient:
    def __init__(self, client_key, client_secret, access_token_url, base_url, institution_filter):
        self.configuration = CardClientConfiguration(
            client_key, client_secret, access_token_url, base_url
        )

        self.card_api_client = ApiClient(self.configuration)
        self.card_api_instance = CardApi(self.card_api_client)

        self.card_request_queue = []
        self._workflow_go_state = "CREATING_TODO"
        self.institution_filter = institution_filter

    @retry(
        reraise=True,
        wait=wait_exponential(multiplier=MULTIPLIER, min=WAIT_MIN, max=WAIT_MAX),
        stop=stop_after_attempt(MAX_ATTEMPT),
    )
    def _refresh_card_request_queue(self, queue_size=200):
        """Fetch the next group of card requests from the card API"""

        LOG.info("Updating card request queue...")

        if self.institution_filter:
            response = self.card_api_instance.v1beta1_card_requests_list(
                workflow_state=[self._workflow_go_state],
                async_req=False,
                destination=self.institution_filter.upper(),
                page_size=queue_size,
            )
        else:
            response = self.card_api_instance.v1beta1_card_requests_list(
                workflow_state=[self._workflow_go_state],
                async_req=False,
                page_size=queue_size,
            )

        self.card_request_queue = [
            card_request["id"] for card_request in response.get("results", [])
        ]

    @retry(
        reraise=True,
        wait=wait_exponential(multiplier=MULTIPLIER, min=WAIT_MIN, max=WAIT_MAX),
        stop=stop_after_attempt(MAX_ATTEMPT),
    )
    def _get_card_request_details(self, id):
        """Get the details for a specific card request"""

        LOG.info(f"Fetching card request details {id}")

        response = self.card_api_instance.v1beta1_card_requests_read(id, async_req=False)

        return response

    @retry(
        reraise=True,
        wait=wait_exponential(multiplier=MULTIPLIER, min=WAIT_MIN, max=WAIT_MAX),
        stop=stop_after_attempt(MAX_ATTEMPT),
    )
    def _fetch_image(self, image_url: str):
        """Write data from api response to local bmp file"""

        img_resource_path = re.sub(
            r"^{0}".format(re.escape(self.configuration.host)), "", image_url
        )

        LOG.debug(f"Fetching image {img_resource_path}")

        response, response_code, _ = self.card_api_instance.api_client.call_api(
            resource_path=img_resource_path,
            method="GET",
            header_params={
                "Authorization": f"Bearer {self.configuration.access_token}",
                "Accept": "image/bmp",
            },
            query_params={"render_placeholder": "false"},
            response_type=(IOBase,),
        )

        assert response_code == 200

        image_file = tempfile.NamedTemporaryFile(mode="w+b", suffix=".bmp", delete=False)
        image_file.write(response.read())
        image_file.close()

        LOG.debug(f"Image saved to local path {image_file.name}")

        return image_file.name

    def get_single_card_request(self, card_request_id: str, rfid_data_format: str):
        """Return the data associated with a single card request."""

        self.update_card_request(card_request_id, {"action": "start"})

        try:
            # Refresh the card request from the data sources, raising exception
            # if card request is no longer ready for creation.
            self.update_card_request(card_request_id, {"action": "refresh"})
            card_request_details = self._get_card_request_details(card_request_id)

            rfid_datas = list(
                filter(
                    lambda item: str(item["id"]) == str(rfid_data_format),
                    card_request_details["rfid_data"],
                )
            )

            if len(rfid_datas) != 1:
                raise RuntimeError(
                    f"card_request {card_request_details['id']} "
                    f"did not return a unique rfid_data for key: {rfid_data_format}"
                )

            rfid_data = rfid_datas[0]

            card_rfid_configuration = self.get_card_rfid_data_config(id=rfid_data["configuration"])

            for block in rfid_data["blocks"]:
                block["data"] = to_hexstring(block["data"])

            merged_rfid_data = merge_data(rfid_data["blocks"], card_rfid_configuration)

            LOG.info(f"Fetching card request {card_request_details['id']} front image.")
            front_image = self._fetch_image(card_request_details["front_visualization_link"])

            LOG.info(f"Fetching card request {card_request_details['id']} back image.")
            back_image = self._fetch_image(card_request_details["back_visualization_link"])

        except Exception as e:
            # Abandon card request and return to STATE_PENDING if card request
            # is no longer ready for creation.
            self.update_card_request(card_request_id, {"action": "abandon"})
            raise e

        return card_request_details, merged_rfid_data, front_image, back_image

    def get_next_card_request(self, rfid_data_format: str):
        """Get data from the next card request in the card request queue."""

        if not self.card_request_queue:
            try:
                self._refresh_card_request_queue()
            except Exception as e:
                LOG.error(f"API gateway returned exception {e}")
                self.card_request_queue = []

        if not self.card_request_queue:
            return None, None, None, None

        while self.card_request_queue:
            # loop until card request data is returned or queue is empty
            try:
                (
                    card_request_details,
                    merged_rfid_data,
                    front_image,
                    back_image,
                ) = self.get_single_card_request(self.card_request_queue[0], rfid_data_format)
                return card_request_details, merged_rfid_data, front_image, back_image
            except Exception as e:
                LOG.error(f"card request {self.card_request_queue[0]} raised exception: {e}")
                continue
            finally:
                self.card_request_queue.pop(0)

        # case where no card requests can be started due to API raising exceptions
        return None, None, None, None

    @retry(
        reraise=True,
        retry=retry_if_not_exception_type(ApiException),
        wait=wait_exponential(multiplier=MULTIPLIER, min=WAIT_MIN, max=WAIT_MAX),
        stop=stop_after_attempt(MAX_ATTEMPT),
    )
    def update_card_request(self, id, body):
        """Update the card request"""

        LOG.info(f"Updating card request {id} {body}")

        try:
            response = self.card_api_instance.v1beta1_card_requests_update(
                id=id, data=body, async_req=False
            )

        except ApiException as e:
            LOG.error(f"card request {self.card_request_queue[0]} returned exception: {e.body}")
            raise e

        return response

    @retry(
        reraise=True,
        wait=wait_exponential(multiplier=MULTIPLIER, min=WAIT_MIN, max=WAIT_MAX),
        stop=stop_after_attempt(MAX_ATTEMPT),
    )
    def get_card_rfid_data_config(self, id):
        """Fetch the card RFID data configuration from the Card API"""

        LOG.info(f"Fetching RFID data configuration: {id}")

        response = self.card_api_instance.v1beta1_card_rfid_data_config_list(async_req=False)
        rfid_data_configs = list(
            filter(lambda config: str(config["id"]) == str(id), response["results"])
        )

        if len(rfid_data_configs) != 1:
            raise RuntimeError(
                f"Card API did not return a unique RFID configuration for key: {id}"
            )

        configuration = rfid_data_configs[0]["configuration"]

        return configuration
