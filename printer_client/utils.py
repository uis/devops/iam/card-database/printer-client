import os
import logging
from yaml import safe_load
from deepmerge import always_merger

LOG = logging.getLogger(__name__)


def load_yaml_file(file_path: str) -> dict:
    try:
        with open(file_path, "r") as file_content:
            return safe_load(file_content)
    except IOError:
        raise IOError(f"File does not exist: f{file_path}")


def load_settings(path: str) -> dict:
    settings = {}
    LOG.info("Loading settings from %s", path)
    if not os.path.exists(path):
        raise RuntimeError(f"Configuration file not found! Check path: {path}")

    settings = always_merger.merge(settings, load_yaml_file(path))

    return settings or {}
