from unittest import TestCase
import tempfile

from printer_client.utils import load_yaml_file


class TestUtils(TestCase):
    def test_load_yaml_file(self):
        file_content = "client_key: client_key"
        temp_file = tempfile.NamedTemporaryFile(mode="w+")
        temp_file.write(file_content)
        temp_file.flush()

        expected = {"client_key": "client_key"}
        actual = load_yaml_file(temp_file.name)

        self.assertEqual(expected, actual)
