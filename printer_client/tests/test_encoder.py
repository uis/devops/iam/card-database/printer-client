from unittest import TestCase, mock

from printer_client.encoder import (
    generate_uid_hash,
    read_card_uid,
    block_operation,
    card_operation,
    merge_data,
    to_ascii,
    to_bytearray,
    to_hexstring,
    to_integer,
)


class TestEncoder(TestCase):
    def test_to_bytearray(self):
        actual = to_bytearray("13232000000000000000000000000000")
        expected = bytearray(b"\x13# \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")

        self.assertEqual(expected, actual)

    def test_to_hexstring(self):
        actual = to_hexstring(36004003)
        self.assertEqual("a3602502000000000000000000000000", actual)

        actual = to_hexstring("jd0123j")
        self.assertEqual("6a64303132336a000000000000000000", actual)

    def test_to_ascii(self):
        actual = to_ascii([110, 119, 48, 52, 48, 48, 98, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        expected = "nw0400b"

        self.assertEqual(actual, expected)

    def test_to_integer(self):
        actual = to_integer([110, 119, 48, 52, 48, 48, 98, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        expected = 27637531309799278

        self.assertEqual(actual, expected)

    def test_merge_data(self):
        """Test merge of left (RFID data) into right (RFID configuration)"""

        # card request data
        input_left = [{"block": 0, "data": "FFFFFFFFFFFFFF078069FFFFFFFFFFFF"}]

        # card RFID configuration
        input_right = [
            {
                "block": 0,
                "readkey": "FFFFFFFFFFFF",
                "writekey": "FFFFFFFFFFFF",
                "data": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
            },
            {
                "block": 1,
                "readkey": "FFFFFFFFFFFF",
                "writekey": "FFFFFFFFFFFF",
                "data": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
            },
        ]

        expected = [
            {
                "block": 0,
                "readkey": "FFFFFFFFFFFF",
                "writekey": "FFFFFFFFFFFF",
                "data": "FFFFFFFFFFFFFF078069FFFFFFFFFFFF",
            },
            {
                "block": 1,
                "readkey": "FFFFFFFFFFFF",
                "writekey": "FFFFFFFFFFFF",
                "data": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
            },
        ]

        actual = merge_data(input_left, input_right)

        self.assertEqual(expected, actual)

    def test_merge_data_raises_exception(self):
        """Test merge_data raises value error if left is not a subset of right"""

        # card request data
        input_left = [{"block": 3, "data": "FFFFFFFFFFFFFF078069FFFFFFFFFFFF"}]

        # card RFID configuration
        input_right = [
            {
                "block": 0,
                "readkey": "FFFFFFFFFFFF",
                "writekey": "FFFFFFFFFFFF",
                "data": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
            },
            {
                "block": 1,
                "readkey": "FFFFFFFFFFFF",
                "writekey": "FFFFFFFFFFFF",
                "data": "FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF",
            },
        ]

        with self.assertRaises(ValueError):
            merge_data(input_left, input_right)

    def test_generate_uid_hash(self):
        input = 100
        expected = 77816838
        actual = generate_uid_hash(input)

        self.assertEqual(expected, actual)

    @mock.patch("printer_client.encoder.CardRequest")
    def test_read_card_uid(self, mock_hardware):
        """Mock the card encoder and simulate reading the card card_uid"""

        mock_hardware.connection.transmit.return_value = [
            (123).to_bytes(16, byteorder="little"),
            int("0x90", 16),
            None,
        ]

        expected = 123
        actual = read_card_uid(mock_hardware)

        self.assertEqual(expected, actual)

    @mock.patch("printer_client.encoder.CardRequest")
    def test_block_operation_write(self, mock_hardware):
        """Mock the encoder and simulate a successful block write"""

        mock_hardware.connection.transmit.return_value = [None, int("0x90", 16), None]

        input = {
            "block": 0,
            "readkey": "FFFFFFFFFFFF",
            "writekey": "FFFFFFFFFFFF",
            "data": "FFFFFFFFFFFFFF078069FFFFFFFFFFFF",
            "authwith": "readkey",
        }

        expected = None
        actual = block_operation(
            mock_hardware,
            input["data"],
            input["block"],
            input["readkey"],
            input["writekey"],
            input["authwith"],
            operation="write",
        )

        self.assertEqual(expected, actual)

    @mock.patch("printer_client.encoder.CardRequest")
    def test_block_operation_read(self, mock_hardware):
        """Mock the encoder and simulate a successful block write"""

        mock_hardware.connection.transmit.return_value = [
            bytearray(b"\x13# \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"),
            int("0x90", 16),
            None,
        ]

        input = {
            "block": 0,
            "readkey": "FFFFFFFFFFFF",
            "writekey": "FFFFFFFFFFFF",
            "data": "FFFFFFFFFFFFFF078069FFFFFFFFFFFF",
            "authwith": "readkey",
        }

        expected = bytearray(b"\x13# \x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
        actual = block_operation(
            mock_hardware,
            input["data"],
            input["block"],
            input["readkey"],
            input["writekey"],
            input["authwith"],
        )

        self.assertEqual(expected, actual)

    @mock.patch("printer_client.encoder.CardRequest")
    def test_card_operation(self, mock_hardware):
        """Mock the encoder and simulate a successful card write"""

        mock_hardware.connection.transmit.return_value = [None, int("0x90", 16), None]

        input = {
            "block": 0,
            "readkey": "FFFFFFFFFFFF",
            "writekey": "FFFFFFFFFFFF",
            "data": "FFFFFFFFFFFFFF078069FFFFFFFFFFFF",
            "authwith": "readkey",
        }

        expected = None
        actual = card_operation(input, None)

        self.assertEqual(expected, actual)
