from unittest import TestCase, mock

from printer_client.printer import create_card, encode_blank_card


class TestPrinter(TestCase):
    @mock.patch("printer_client.printer.subprocess.Popen", autospec=False)
    @mock.patch("printer_client.printer.card_operation")
    def test_create_card(self, mock_encoder, mock_subprocess):
        """
        Test create_card returns the card_uid by mocking the encoder and zebra
        printing  subprocess.
        """

        mock_instance = mock.Mock()
        mock_instance.poll.side_effect = iter([None, None, None])
        mock_instance.stdout.readline.side_effect = iter(["[CARD_AT_ENCODER]", "[COMPLETE]"])
        mock_instance.returncode = 0

        mock_subprocess.return_value = mock_instance
        mock_encoder.return_value = 12345678
        mock_merged_bytedata = [
            {
                "block": 0,
                "readkey": [255, 255, 255, 255, 255, 255],
                "writekey": [0, 0, 0, 0, 0, 0],
                "data": [255, 0, 0, 0, 0, 255],
            }
        ]

        card_uid = create_card(
            mock_merged_bytedata, "file:/mock_front_path", "file:/mock_back_path"
        )

        self.assertEqual(12345678, card_uid)

    @mock.patch("printer_client.printer.subprocess.Popen", autospec=False)
    @mock.patch("printer_client.printer.card_operation")
    def test_create_card_mifare_number_uid_hash(self, mock_encoder, mock_subprocess):
        """
        Test create_card with overwrite of MIFARE_NUMBER by UID hash returns
        the card_uid by mocking the encoder and zebra printing  subprocess.
        """

        mock_instance = mock.Mock()
        mock_instance.poll.side_effect = iter([None, None, None])
        mock_instance.stdout.readline.side_effect = iter(["[CARD_AT_ENCODER]", "[COMPLETE]"])
        mock_instance.returncode = 0

        mock_subprocess.return_value = mock_instance
        mock_encoder.return_value = 12345678
        mock_merged_bytedata = [
            {
                "block": 6,
                "readkey": [255, 255, 255, 255, 255, 255],
                "writekey": [0, 0, 0, 0, 0, 0],
                "data": [0, 0, 0, 0, 0, 0],
            },
            {
                "block": 18,
                "readkey": [255, 255, 255, 255, 255, 255],
                "writekey": [0, 0, 0, 0, 0, 0],
                "data": [0, 0, 0, 0, 0, 0],
            },
        ]

        card_uid = encode_blank_card(mock_merged_bytedata)

        calls = [
            mock.call([], None, "read", "SDI011"),
            mock.call(
                [
                    {
                        "block": 6,
                        "readkey": [255, 255, 255, 255, 255, 255],
                        "writekey": [0, 0, 0, 0, 0, 0],
                        "data": "4f236c04000000000000000000000000",
                    },
                    {
                        "block": 18,
                        "readkey": [255, 255, 255, 255, 255, 255],
                        "writekey": [0, 0, 0, 0, 0, 0],
                        "data": "4f236c04000000000000000000000000",
                    },
                ],
                None,
                "write",
                "SDI011",
            ),
        ]

        mock_encoder.assert_has_calls(calls)
        self.assertEqual(12345678, card_uid)
