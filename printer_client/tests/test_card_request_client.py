from unittest import TestCase, mock

from printer_client.card_request_client import CardRequestClient
from printer_client.tests.mock_api_clients import (
    MockApiClient,
    MockCardApi,
    MockConfiguration,
)


class TestCardRequestClient(TestCase):
    @mock.patch(
        "printer_client.card_request_client.CardRequestClient._fetch_image",
        return_value="image_url",
    )
    @mock.patch(
        "printer_client.card_request_client.CardClientConfiguration",
        return_value=MockConfiguration(),
    )
    @mock.patch("printer_client.card_request_client.ApiClient", return_value=MockApiClient())
    @mock.patch("printer_client.card_request_client.CardApi", return_value=MockCardApi())
    def setUp(self, *args, **kwargs):
        """create a mock card request client"""

        self.card_request_client = CardRequestClient(
            "client_key",
            "client_secret",
            "access_token_url",
            "base_url",
            "institution_filter",
        )

    def test_refresh_card_request_queue(self):
        """Test card queue refresh returns the card request mock"""

        self.card_request_client._refresh_card_request_queue()

        self.assertEqual(
            [
                "00000001-0000-0000-0000-0000000000000",
                "00000002-0000-0000-0000-0000000000000",
            ],
            self.card_request_client.card_request_queue,
        )

    def test_get_card_request_details(self):
        """Test mock card request details are returned"""

        card_request_details = self.card_request_client._get_card_request_details(
            "00000001-0000-0000-0000-0000000000000"
        )

        self.assertEqual("00000001-0000-0000-0000-0000000000000", card_request_details["id"])
        self.assertEqual(card_request_details["workflow_state"], "CREATING_TODO")

    def test_get_next_card_request(self):
        """Test the next mock card request is returned"""

        (
            card_request_details,
            merged_rfid_data,
            front_image,
            back_image,
        ) = self.card_request_client.get_next_card_request("MIFARE_V1")

        self.assertEqual(card_request_details["id"], "00000001-0000-0000-0000-0000000000000")
        self.assertEqual(card_request_details["workflow_state"], "CREATING_TODO")

        block = list(filter(lambda item: item["block"] == 4, merged_rfid_data))

        self.assertEqual(block[0]["data"], "6a643030303078000000000000000000")

        self.assertRegex(front_image, "/tmp/")
        self.assertRegex(front_image, ".bmp")
        self.assertRegex(back_image, "/tmp/")
        self.assertRegex(back_image, ".bmp")

    def test_get_single_card_request(self):
        """Test a single mock card request is returned"""

        (
            card_request_details,
            merged_rfid_data,
            front_image,
            back_image,
        ) = self.card_request_client.get_single_card_request(
            "00000001-0000-0000-0000-0000000000000", "MIFARE_V1"
        )

        self.assertEqual(card_request_details["id"], "00000001-0000-0000-0000-0000000000000")
        self.assertEqual(card_request_details["workflow_state"], "CREATING_TODO")

        block = list(filter(lambda item: item["block"] == 4, merged_rfid_data))

        self.assertEqual(block[0]["data"], "6a643030303078000000000000000000")

        self.assertRegex(front_image, "/tmp/")
        self.assertRegex(front_image, ".bmp")
        self.assertRegex(back_image, "/tmp/")
        self.assertRegex(back_image, ".bmp")

    def test_update_card_request(self):
        """Test a mock of the card request update"""

        return_value = self.card_request_client.update_card_request(
            "00000001-0000-0000-0000-0000000000000", {"action": "start"}
        )

        self.assertEqual([], return_value["results"])

    def test_fetch_image(self):
        """Test the bmp mock is written to a tempfile"""

        img_tempfile = self.card_request_client._fetch_image("target_image_url")

        # example: /tmp/tmpzdzzj0lx.bmp
        self.assertRegex(img_tempfile, "/tmp/")
        self.assertRegex(img_tempfile, ".bmp")

    def test_get_rfid_config(self):
        """Test get_rfid_data_config returns mock config"""

        rfid_data_config = self.card_request_client.get_card_rfid_data_config(
            "MIFARE_CLASSIC_4K_V1"
        )

        self.assertEqual(
            [
                {
                    "block": 4,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 5,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 6,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 8,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 9,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 10,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 12,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 13,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 14,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 16,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 17,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
                {
                    "block": 18,
                    "authwith": "readkey",
                    "readkey": "FFFFFFFFFFFF",
                    "writekey": "FFFFFFFFFFFF",
                    "data": "00000000000000000000000000000000",
                },
            ],
            rfid_data_config,
        )

    @mock.patch("tenacity.nap.time.sleep", mock.MagicMock())
    def test_get_rfid_config_raises_exception(self):
        """Check a junk configuration id raises an exception"""

        self.assertRaises(
            RuntimeError, self.card_request_client.get_card_rfid_data_config, "junk_id"
        )
