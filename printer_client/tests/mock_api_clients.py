from io import BytesIO
from PIL import Image, ImageDraw


class MockConfiguration:
    """Mock the Identitylib Card API configuration"""

    def __init__(self, *args, **kwargs):
        self.host = "mock_host"
        self.access_token = "mock_access_token"

    def __iter__(self):
        return self

    def __next__(self):
        return self


class MockApiClient:
    """Mock the Identitylib Card API client"""

    def __init__(self, *args, **kwargs):
        pass

    def call_api(self, *args, **kwargs):
        """Mock the direct Identitylib call_api returning a BytesIO bmp object"""

        image = Image.new("RGB", (200, 200))
        draw = ImageDraw.Draw(image)
        draw.text((0, 0), "sample text")
        byte_io = BytesIO()
        image.save(byte_io, "BMP")

        return (byte_io, 200, None)


class MockCardApi:
    """Mock the Identitylib Card API"""

    def __init__(self, *args, **kkwargs):
        self.api_client = MockApiClient()

    def __iter__(self):
        return self

    def __next__(self):
        return self

    def v1beta1_card_requests_update(self, id, data, *args, **kwargs):
        return {"next": None, "previous": None, "results": []}

    def v1beta1_card_requests_list(self, *args, **kwargs):
        return {
            "next": None,
            "previous": None,
            "results": [
                {
                    "id": "00000001-0000-0000-0000-0000000000000",
                    "self_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000001-0000-0000-0000-0000000000000",  # noqa: E501
                    "front_visualization_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000001-0000-0000-0000-0000000000000/front-visualization",  # noqa: E501
                    "back_visualization_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000001-0000-0000-0000-0000000000000/back-visualization",  # noqa: E501
                    "card_type": "MIFARE_PERSONAL",
                    "updated_at": "2021-09-14T07:07:20.064345Z",
                    "workflow_state": "CREATING_TODO",
                },
                {
                    "id": "00000002-0000-0000-0000-0000000000000",
                    "self_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000002-0000-0000-0000-0000000000000",  # noqa: E501
                    "front_visualization_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000002-0000-0000-0000-0000000000000/front-visualization",  # noqa: E501
                    "back_visualization_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000002-0000-0000-0000-0000000000000/back-visualization",  # noqa: E501
                    "card_type": "MIFARE_PERSONAL",
                    "updated_at": "2021-09-15T07:07:00.792628Z",
                    "workflow_state": "CREATING_TODO",
                },
            ],
        }

    def v1beta1_card_requests_read(self, id, *args, **kkwargs):
        return {
            "id": "00000001-0000-0000-0000-0000000000000",
            "self_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000001-0000-0000-0000-0000000000000",  # noqa: E501
            "front_visualization_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000001-0000-0000-0000-0000000000000/front-visualization",  # noqa: E501
            "back_visualization_link": "https://api.apps.cam.ac.uk/card/v1beta1/card-requests/00000001-0000-0000-0000-0000000000000/back-visualization",  # noqa: E501
            "card_type": "MIFARE_PERSONAL",
            "updated_at": "2021-09-14T07:07:20.064345Z",
            "workflow_state": "CREATING_TODO",
            "rfid_data": [
                {
                    "id": "MIFARE_V1",
                    "configuration": "MIFARE_CLASSIC_4K_V1",
                    "blocks": [
                        {"block": 4, "data": "jd0000x"},
                        {"block": 5, "data": "01"},
                        {"block": 6, "data": 24522351},
                        {"block": 8, "data": "JD0000"},
                        {"block": 9, "data": None},
                        {"block": 10, "data": "30042166"},
                        {"block": 12, "data": None},
                        {"block": 13, "data": None},
                        {"block": 14, "data": "VWQ0K"},
                        {"block": 16, "data": "jd0000x"},
                        {"block": 17, "data": "01"},
                        {"block": 18, "data": 24522351},
                    ],
                }
            ],
            "attributes": {
                "name": "J Doe",
                "logos": {
                    "aboveId": None,
                    "topLeft": "00000001-0000-0000-0000-0000000000000@card-logo.v1.card.university.identifiers.cam.ac.uk",  # noqa: E501
                    "topRight": None,
                    "belowAddress": None,
                },
                "photo": None,
                "scarf": "00000001-0000-0000-0000-0000000000000@card-logo.v1.card.university.identifiers.cam.ac.uk",  # noqa: E501
                "barcode": "VWQ0K",
                "expires": "",
                "reference": "jd0000x / 01",
            },
        }

    def v1beta1_card_rfid_data_config_list(self, *args, **kwargs):
        """Return a mock of the card RFID data configuration"""
        return {
            "results": [
                {
                    "id": "MIFARE_CLASSIC_4K_V1",
                    "configuration": [
                        {
                            "block": 4,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 5,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 6,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 8,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 9,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 10,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 12,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 13,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 14,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 16,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 17,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                        {
                            "block": 18,
                            "authwith": "readkey",
                            "readkey": "FFFFFFFFFFFF",
                            "writekey": "FFFFFFFFFFFF",
                            "data": "00000000000000000000000000000000",
                        },
                    ],
                }
            ]
        }
