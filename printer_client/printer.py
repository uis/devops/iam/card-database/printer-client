import os
import subprocess
from typing import List, Union
from logging import getLogger

from printer_client.encoder import (
    card_operation,
    generate_uid_hash,
    merge_data,
    to_hexstring,
)

LOG = getLogger(__name__)


def run_zebra_subprocess(front_path: str, back_path: str) -> None:
    """Run the Zebra printer client (java code) on a subprocess"""

    os.chdir(os.path.join(os.path.dirname(__file__), "javaclient"))

    try:
        command = ["java", "-jar", "./zebraclient_v1.jar", front_path, back_path]
    except FileNotFoundError:
        LOG.error("Zebra .jar file not found.")
        raise RuntimeError("Zebra .jar file not found.")

    process = subprocess.Popen(
        command,
        shell=False,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )

    return process


def encode_blank_card(rfid_sector_data: List[dict]) -> int:
    """
    Encode a blank card, generating a MIFARE_NUMBER from a hash of the card UID
    set by the card manufacturer.

    """

    card_uid = create_card(
        rfid_sector_data, None, None, overwrite_mifare_number_with_uid_hash=True
    )

    return card_uid


def create_card(
    merged_bytedata: List[dict],
    front_path: Union[str, None],
    back_path: Union[str, None],
    overwrite_mifare_number_with_uid_hash: bool = False,
) -> int:
    """Use the Zebra ZXP Series 7 printer to encode and print the card"""

    card_uid = None

    if front_path is None:
        front_path = ""

    if back_path is None:
        back_path = ""

    process = run_zebra_subprocess(front_path, back_path)

    while True:
        output = process.stdout.readline()

        if process.poll() is not None:
            break

        if output:
            LOG.debug(output)
            if "[CARD_AT_ENCODER]" in str(output.strip()):
                try:
                    if overwrite_mifare_number_with_uid_hash:
                        # Force overwrite the MIFARE_NUMBER in block 6 and 18 with
                        # a hash generated from the card UID set by the manufacturer.
                        card_uid = card_operation([], None, "read", "SDI011")
                        uid_hash = to_hexstring(generate_uid_hash(card_uid))

                        merged_bytedata = merge_data(
                            [
                                {"block": 6, "data": uid_hash},
                                {"block": 18, "data": uid_hash},
                            ],
                            merged_bytedata,
                        )

                    card_uid = card_operation(merged_bytedata, None, "write", "SDI011")
                    LOG.info(f"Encoding complete, card uid: {card_uid}")
                    process.stdin.write(b"RESUME\n")
                    process.stdin.flush()

                except Exception as err:
                    process.stdin.write(b"CANCEL\n")
                    process.stdin.flush()
                    process.wait()
                    raise RuntimeError(str(err))

            elif any(i in str(output.strip()) for i in ["[ABORTED]", "[ERROR]", "[ALARM]"]):
                LOG.error(output)
                process.wait()
                raise RuntimeError(output)

            elif "java.lang.UnsatisfiedLinkError" in str(output.strip()):
                LOG.warning("Make sure the .dll and jar are in the same directory.")
                LOG.error(output)
                process.wait()
                raise RuntimeError(output)

            elif "[COMPLETE]" in str(output.strip()):
                LOG.info(f"Printing complete, card uid: {card_uid}")
                process.wait()
                break

    process.wait()

    if process.returncode != 0:
        raise RuntimeError("Hard printer subprocess error.")

    return card_uid
