function Install-PrinterClient{
    # PowerShell script used to clone the printer-client repository,
    # install the python requirements and create desktop shortcuts.

    # To bypass the local execution policy run using:
    # > powershell -ExecutionPolicy Bypass -File .\install_printer_client.ps1

    $remotePath = "https://gitlab.developers.cam.ac.uk/uis/devops/iam/card-database/printer-client.git"

    $localPath = "C:\repo\printer-client\"
    $localRequirementsFile = "$localPath\requirements\base.txt"

    $localPrinterClientRunPath = "$localPath\run_printer_client.bat"
    $localPrinterClientShortcutPath = "$env:Public\Desktop\Run_Printer_Client.lnk"

    $localEncoderClientRunPath = "$localPath\run_encoder_client.bat"
    $localEncoderClientShortcutPath = "$env:Public\Desktop\Run_Encoder_Client.lnk"

    $localInstallFlag = "$localPath\.printer-client-installed"

    # $localPath does not exist, clone $remotePath to $localPath
    if ( -not( Test-Path -Path $localPath ) ) {
        try {
            # create a local folder
            New-Item -ItemType Directory -Force -Path $localPath

            # clone remote repository
            git clone $remotePath $localPath
        }
        catch {
            Write-Warning "install-printer-client failed to clone"
            Remove-Item -LiteralPath $localPath -Force -Recurse
            exit 1
        }
    }

    # if $localInstallFlag does not exist, install the python $localRequirementsFile
    if ( -not( Test-Path -Path $localInstallFlag ) ) {
        try {
            # git pull the latest main branch
            Set-Location $localPath
            git pull origin main

            # install python requirements
            python -m pip install --upgrade pip
            python -m pip install -r $localRequirementsFile

            # if $localPrinterClientShortcutPath does not exist, create shortcut
            if ( -not( Test-Path -Path $localPrinterClientShortcutPath ) ) {
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut($localPrinterClientShortcutPath)
                $Shortcut.TargetPath = $localPrinterClientRunPath
                $Shortcut.Save()
            }

            # if $localEncoderClientShortcutPath does not exist, create a shortcut
            if ( -not( Test-Path -Path $localEncoderClientShortcutPath ) ) {
                $WshShell = New-Object -comObject WScript.Shell
                $Shortcut = $WshShell.CreateShortcut($localEncoderClientShortcutPath)
                $Shortcut.TargetPath = $localEncoderClientRunPath
                $Shortcut.Save()
            }

            # write the install flag if successful
            New-Item $localInstallFlag
        }
        catch {
            Write-Warning "install-printer-client failed to install"
            exit 1
        }
    }
}

Install-PrinterClient
