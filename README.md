# Printer Client

This repository contains a client application that connects the Zebra ZXP
Series 7 printers (with embedded Identiv card encoder) to the Card API.

The Card API provides the rendered artwork for printing on the card front and
and back along with the RFID data that is encoded on the Mifare 4K card.

The client code returns the card uid (Mifare ID) to the Card API for
cards that successfully complete encoding and printing.

The printer client calls a Java subprocess to manage all communications with the
Zebra ZXP Series 7 printer. The SDI011 Identiv encoder - embedded in the Series 7
printer - is controlled directly by the printer client.

# Development

## System Configuration

The Zebra ZXP printer SDK supports multiple operating systems, the following
outlines the current development environment but is not intended as prescriptive:

- Windows 10 64-bit
- Python 3.9 64-bit
- swig (link)[http://www.swig.org/]
- Zebra ZXP Series 7 Printer Driver dz7cg-03-02-00 (released 31-MAR-2016) (link)[https://www.zebra.com/gb/en/support-downloads/printers/card/zxp-series-7.html#drivers]
- ZebraNativeUsbAdapter_32.dll (see: `printer_client/javaclient/`)
- Zebra Link OS Java SDK, Build: 2.12.3782 (link)[https://www.zebra.com/gb/en/support-downloads/printer-software/developer-tools/card-sdk.html]
- Java SE Development Kit 1.8 (Build: 1.8.0_301) (link)[https://www.oracle.com/java/technologies/downloads/#java8-windows]
- Java(TM) SE Runtime Environment (build 1.8.0_301-b09)

Note: Java needs to be pinned to this version. Anything that will lead to the Java
being upgraded on the local system (e.g. automated or user prompted upgrade scripts)
will need to be disabled.

Eclipse 2021-09 was used as the IDE.

# Running Tests

Tests can be run using the `./test.sh` command:

```bash
# Run PyTest and Flake8:
$ ./test.sh

# Run PyTest and Flake8 and force rebuild:
$ ./test.sh --recreate

# Run PyTest only:
$ ./test.sh -e py3

# Run a single test file within PyTest:
$ ./test.sh -e py3 -- tests/test_[testname].py

# Run a single test file within PyTest with verbose logging
$ ./test.sh -e py3 -- tests/test_[testname].py -vvv
```

# Running as Python module

```bash
# Run the printer client as a module from this repository:
python -m printer_client run

# Run with debug output enabled
python -m printer_client run --debug
```

# Production

## System Configuration

- Windows 10 64-bit
- Python 3.9 64-bit
- Zebra ZXP Series 7 Printer Driver dz7cg-03-02-00 (released 31-MAR-2016)
- Java 1.8 (supports both 32-bit and 64-bit)
- Swigwin 4.1.0

## Deployment (Manual)

The approximate steps to deploy a local instance:

- Install the dependencies listed above
- Install the printer client app `pip install .` or `python3 setup.py install`
- Connect the Zebra ZXP Series 7 printer via USB (encoding via Ethernet not supported)

- Create a new app via the API Gateway, the app must have write access to the Card API
- Create a `config.yml` and add the app API key and secret (see: `config.yml.example`)
- Run the `run.bat` script to start the printer client.

## Deployment (Microsoft Intune)

The UIS service desk has automated the deployment using Microsoft Intune.

The steps needed to deploy the printer client to an additional endpoint are:

- Add the target machine to the CardServicesGroup
- Login to the machine and wait until the system auto-configures (may need a power cycle)
- Create a new config file at `c:\repo\printer-client\config.yml` (see example)

## Copyright

The Zebra SDK examples remain the property of ZIH Corp and is subject to the
terms and conditions in the Zebra end user license agreement.

Java remains the property of Oracle. You may require a license to deploy to production.
https://stackoverflow.com/questions/58250782/which-free-version-of-java-can-i-use-for-production-environments-and-or-commerci
