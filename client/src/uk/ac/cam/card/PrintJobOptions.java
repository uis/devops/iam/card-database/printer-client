package uk.ac.cam.card;

import java.util.HashMap;
import java.util.Map;

import com.zebra.sdk.common.card.enumerations.PrintType;

public class PrintJobOptions {

    public boolean frontSelected = false;
    public boolean backSelected = false;
    public Map<PrintType, String> frontImageInfo = new HashMap<PrintType, String>();
    public Map<PrintType, String> backImageInfo = new HashMap<PrintType, String>();
    public int copies = 1;
}
