package uk.ac.cam.card;

import com.zebra.sdk.common.card.enumerations.PrintType;
import uk.ac.cam.card.PrintJobOptions;
import uk.ac.cam.card.Printer;


public class ZebraClient {
    static String frontSideImageFile = null;
    static String backSideImageFile = null;

    public static void main(String[] args) {

        if (args.length > 0 && !args[0].isEmpty()) {
            frontSideImageFile = args[0];
        }
        if (args.length > 1 && !args[1].isEmpty()) {
            backSideImageFile = args[1];
        }

        System.out.println("Zebra ZXP 7 Printer Client");

        PrintJobOptions printJobOptions = new PrintJobOptions();
        if (frontSideImageFile != null) {
            printJobOptions.frontImageInfo.put(PrintType.Color, frontSideImageFile);
        }
        if (backSideImageFile != null) {
            printJobOptions.backImageInfo.put(PrintType.MonoK, backSideImageFile);
        }

        Printer printer = new Printer();
        printer.discoverPrinter();

        printer.runOperation(printJobOptions);
    }
}
