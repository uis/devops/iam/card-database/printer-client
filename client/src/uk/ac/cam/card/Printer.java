package uk.ac.cam.card;

import java.util.Scanner;
import java.util.Set;

import org.apache.commons.io.FileUtils;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.lang.IllegalStateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;

import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.common.card.containers.GraphicsInfo;
import com.zebra.sdk.common.card.containers.JobStatusInfo;
import com.zebra.sdk.common.card.containers.JobStatus;
import com.zebra.sdk.common.card.enumerations.CardSide;
import com.zebra.sdk.common.card.enumerations.GraphicType;
import com.zebra.sdk.common.card.enumerations.OrientationType;
import com.zebra.sdk.common.card.enumerations.PrintType;
import com.zebra.sdk.common.card.errors.ZebraCardErrors;
import com.zebra.sdk.common.card.exceptions.ZebraCardException;
import com.zebra.sdk.common.card.graphics.ZebraCardGraphics;
import com.zebra.sdk.common.card.graphics.ZebraCardImageI;
import com.zebra.sdk.common.card.graphics.enumerations.RotationType;
import com.zebra.sdk.common.card.jobSettings.ZebraCardJobSettingNames;
import com.zebra.sdk.common.card.printer.ZebraCardPrinter;
import com.zebra.sdk.common.card.printer.ZebraCardPrinterFactory;
import com.zebra.sdk.printer.discovery.DiscoveredUsbPrinter;
import com.zebra.sdk.printer.discovery.UsbDiscoverer;

public class Printer {

    protected Connection connection;
    public static final String USB_SELECTION = "USB";
    public static final Integer CARD_FEED_TIMEOUT = 30000;

    public static String cardSource = "Feeder";
    public static String cardDestination = "Eject";
    public static String cardType = "Contactless";

    private static final String RESUME_OPTION = "RESUME";
    private static final String CANCEL_OPTION = "CANCEL";
    private static final Set<String> OPTIONS = new HashSet<String>(Arrays.asList(RESUME_OPTION, CANCEL_OPTION));

    public DiscoveredUsbPrinter printer = null;


    protected boolean pollJobStatus(ZebraCardPrinter zebraCardPrinter, int jobId) throws ConnectionException, ZebraCardException {
        boolean done = false;
        boolean jobSuccessful = false;

        long start = System.currentTimeMillis();

        System.out.println("Polling status for job id " + jobId + "...");

        while (!done) {
            JobStatusInfo jobStatus = zebraCardPrinter.getJobStatus(jobId);

            String alarmDesc = jobStatus.alarmInfo.value > 0 ? " (" + jobStatus.alarmInfo.description + ")" : "";
            String errorDesc = jobStatus.errorInfo.value > 0 ? " (" + jobStatus.errorInfo.description + ")" : "";

            System.out.println(String.format("Job %d: status:%s, position:%s, contact:%s, contactless:%s, alarm:%d%s, error:%d%s", jobId, jobStatus.printStatus, jobStatus.cardPosition,
                    jobStatus.contactSmartCard, jobStatus.contactlessSmartCard, jobStatus.alarmInfo.value, alarmDesc, jobStatus.errorInfo.value, errorDesc));

            if (jobStatus.printStatus.contains("done_ok")) {
                jobSuccessful = true;
                done = true;
            } else if (jobStatus.printStatus.contains("cancelled")) {
                
                System.err.println("Cancelled: Job ID " + jobId);
                done = true;
            } else if (jobStatus.printStatus.contains("error")) {
                zebraCardPrinter.cancel(jobId); // cancel job to ensure its removed from the queue
                System.err.println("[ERROR]: " + jobStatus.errorInfo.description + " Job: " + jobId);
                done = true;
            } else if (jobStatus.alarmInfo.value > 0) {
                zebraCardPrinter.cancel(jobId); // treat all Alarms (e.g. feeder empty) as hard errors
                System.err.println("[ALARM]: " + jobStatus.alarmInfo.description + " Job: " + jobId);
                done = true;
                // waitForUserInput(zebraCardPrinter, jobId, jobStatus.alarmInfo.description);
            } else if (jobStatus.contactSmartCard.contains("at_station") || jobStatus.contactlessSmartCard.contains("at_station")) {
                waitForUserInput(zebraCardPrinter, jobId, "CARD_AT_ENCODER");
            } else if ((jobStatus.printStatus.contains("in_progress") && jobStatus.cardPosition.contains("feeding")) // ZMotif printers
                    || (jobStatus.printStatus.contains("alarm_handling") && jobStatus.alarmInfo.value == ZebraCardErrors.MEDIA_OUT_OF_CARDS)
                    || (jobStatus.printStatus.contains("not_in_printer"))) { // ZXP printers
                if (System.currentTimeMillis() > start + CARD_FEED_TIMEOUT) {
                    zebraCardPrinter.cancel(jobId);
                    System.err.println("[ERROR]: card feed timed out. Job: " + jobId);
                    done = true;
                }
            }

            if (!done) {
                sleep(500);
            }
        }
        return jobSuccessful;
    }

    public void runOperation(PrintJobOptions printJobOptions) {

        ZebraCardGraphics graphics = null;
        ZebraCardPrinter zebraCardPrinter = null;
        Connection connection = null;

        try {
            connection = printer.getConnection();
            connection.open();

            zebraCardPrinter = ZebraCardPrinterFactory.getInstance(connection);
            
            zebraCardPrinter.cancel(0);  // cancel all jobs in progress
            zebraCardPrinter.ejectCard();  // eject any cards in printer module (no affect on encoder module)

            for (JobStatus jobId : zebraCardPrinter.getJobList()) {
                System.out.println("In memory, Job: " + jobId.jobId + ", Status: " + jobId.printStatus);
            }

            // Setup artwork front & back
            graphics = new ZebraCardGraphics(zebraCardPrinter); // Initialize graphics for ZXP Series printers

            byte[] frontSideImageData = null;
            byte[] backSideImageData = null;

            PrintType printType = null;

            if (printJobOptions.frontImageInfo.containsKey(PrintType.Color)) {
                frontSideImageData = FileUtils.readFileToByteArray(new File(printJobOptions.frontImageInfo.get(PrintType.Color)));
                printType = PrintType.Color;
            } else if (printJobOptions.frontImageInfo.containsKey(PrintType.MonoK)) {
                frontSideImageData = FileUtils.readFileToByteArray(new File(printJobOptions.frontImageInfo.get(PrintType.MonoK)));
                printType = PrintType.MonoK;
            }

            if (printJobOptions.backImageInfo.containsKey(PrintType.MonoK)) {
                backSideImageData = FileUtils.readFileToByteArray(new File(printJobOptions.backImageInfo.get(PrintType.MonoK)));
            }

            List<GraphicsInfo> graphicsData = new ArrayList<GraphicsInfo>();

            if (frontSideImageData != null) {
                graphics.initialize(0, 0, OrientationType.Landscape, printType, Color.WHITE);
                graphics.drawImage(frontSideImageData, 0, 0, 0, 0, RotationType.RotateNoneFlipNone);
                graphicsData.add(buildGraphicsInfo(graphics.createImage(), CardSide.Front, printType));
                graphics.clear();
            }

            if (backSideImageData != null) {
                graphics.initialize(0, 0, OrientationType.Landscape, PrintType.MonoK, Color.WHITE);
                graphics.drawImage(backSideImageData, 0, 0, 0, 0, RotationType.RotateNoneFlipNone);
                graphicsData.add(buildGraphicsInfo(graphics.createImage(), CardSide.Back, PrintType.MonoK));
                graphics.clear();
            }


            if (!graphicsData.isEmpty()) {
                // if printing artwork on front/back do not eject card after encoding
                cardDestination = "Hold";
            }

            zebraCardPrinter.setJobSetting(ZebraCardJobSettingNames.CARD_SOURCE, cardSource);
            zebraCardPrinter.setJobSetting(ZebraCardJobSettingNames.CARD_DESTINATION, cardDestination);

            if (cardType.equalsIgnoreCase("Contact")) {
                zebraCardPrinter.setJobSetting(ZebraCardJobSettingNames.SMART_CARD_CONTACT, "yes");
            } else {
                zebraCardPrinter.setJobSetting(ZebraCardJobSettingNames.SMART_CARD_CONTACTLESS, "MIFARE");
            }

            // run encoder job
            // This does not actually encode the card, but simply moves the card to the encoder,
            // this codebase expects that the actual encoding is managed by an external application
            // or the parent process.

            int jobIdEncode = zebraCardPrinter.smartCardEncode(1);

            if (cardSource.equalsIgnoreCase("atm")) {
                waitForUserInput(zebraCardPrinter, jobIdEncode, "Please place a card into the ATM slot.");
            }

            boolean encodeSuccessful = pollJobStatus(zebraCardPrinter, jobIdEncode);
            if (!encodeSuccessful) {
                System.err.println("[ABORTED]: encode incomplete.");
                return;
            }

            // run print job from encode hold position
            if (!graphicsData.isEmpty()) {
                int jobIdPrint = zebraCardPrinter.print(printJobOptions.copies, graphicsData);
                boolean printSuccessful = pollJobStatus(zebraCardPrinter, jobIdPrint);
                if (!printSuccessful) {
                    System.err.println("[ABORTED]: print incomplete.");
                    return;
                }
                System.out.println("[COMPLETE]");
            }

        } catch (Exception e) {
            System.err.println("[ABORTED]: " + e.getLocalizedMessage());
        } finally {
            Printer.cleanUpQuietly(zebraCardPrinter, connection);
        }
    }

    void discoverPrinter() {
        try {
            for(DiscoveredUsbPrinter discoPrinter : UsbDiscoverer.getZebraUsbPrinters()){
             System.out.println("Discovered printer: " + discoPrinter);
             printer = (DiscoveredUsbPrinter) discoPrinter;
            }
        } catch (ConnectionException e) {
            System.err.println("Unable to discover printers : " + e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public static void waitForUserInput(ZebraCardPrinter zebraCardPrinter, int jobId, String alarmDesc) throws ZebraCardException, ConnectionException {
        Scanner scanner = new Scanner(System.in);
        String option = null;

        try {
            while (true) {
                System.out.println("[" + alarmDesc + "] (RESUME or CANCEL)");
                option = scanner.nextLine().toUpperCase();

                if (OPTIONS.contains(option)) {
                    if (option.equals(CANCEL_OPTION)) {
                        zebraCardPrinter.cancel(jobId);
                    } else {
                        zebraCardPrinter.resume();
                    }
                }
                break;
            }
         }
          catch(IllegalStateException | NoSuchElementException e) {
              System.err.println("System.in was closed; exiting");
          }
    }

    public static void cleanUpQuietly(ZebraCardPrinter zebraCardPrinter, Connection connection) {
        try {
            if (zebraCardPrinter != null) {
                zebraCardPrinter.destroy();
            }
        } catch (ZebraCardException e) {
        }

        try {
            if (connection != null) {
                connection.close();
            }
        } catch (ConnectionException e) {
        }
    }

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
        }
    }

    protected GraphicsInfo buildGraphicsInfo(ZebraCardImageI zebraCardImage, CardSide side, PrintType printType) throws IOException {
        GraphicsInfo grInfo = new GraphicsInfo();
        if (zebraCardImage != null) {
            grInfo.graphicData = zebraCardImage;
            grInfo.graphicType = GraphicType.BMP;
        } else {
            grInfo.graphicType = GraphicType.NA;
        }
        grInfo.side = side;
        grInfo.printType = printType;
        return grInfo;
    }
}
