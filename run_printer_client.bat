@echo off
REM Run script to start `printer_client` module.
REM Script will set environmental variables from `config.yml` if provided.

cd c:\repo\printer-client
python -m printer_client run
pause
