@echo off
REM Run script to start `printer_client` module to encode blanks cards with a MIFARE_NUMBER.
REM Script will set environmental variables from `config.yml` if provided.

cd c:\repo\printer-client
python -m printer_client run --encode-blank-cards
pause
