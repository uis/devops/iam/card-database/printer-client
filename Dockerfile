# This Dockerfile is just used for testing purposes

FROM ubuntu:20.04

WORKDIR /usr/src/app

ADD ./requirements/* ./requirements/

RUN apt-get -y update
RUN apt-get install -y python3 python3-pip libc-dev libpcsclite-dev swig

RUN pip install --upgrade pip \
    pip install --no-cache-dir -r requirements/base.txt \
    pip install --no-cache-dir -r requirements/test.txt

ADD . .